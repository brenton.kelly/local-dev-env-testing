// https://www.npmjs.com/package/sync-request
const request = require('sync-request');

// https://www.npmjs.com/package/inquirer
const inquirer = require('inquirer');

// GET THE localHostPath VALUE FROM package.json
// THE REGEX REMOVES ANY "/" CHARACTERS FROM THE BEGINNING AND END OF THE STRING SO THAT WE CAN WORK WITH A PREDICTABLE VALUE
const pJson = require('../../../package.json');
let localHostPath = pJson.localHostPath.replace(/(^\/?)(.*?)(\/?$)/gi, '$2');
if(localHostPath == "") {
    localHostPath = "/";
} else {
    localHostPath = "/" + localHostPath + "/";
}

// COMMAND LINE PROMPTS
inquirer.prompt([
    {
        type: "input",
        name: "template-name",
        message: "Template Name"
    },
    {
        type: "editor",
        name: "template-description",
        message: "Template Description"
    },
    {
        type: "input",
        name: "asset-path",
        message: "Template Virtual Asset Path",
        default: "lib9/SWCS000009"
    }
]).then(answers => {
    // NODE SYNCHRONOUS AJAX REQUEST
    let res = request('POST', 'http://localhost:8888' + localHostPath + 'src/build/build-template/build.php', {
        headers: {       
            'content-type': 'application/x-www-form-urlencoded'
        },
        body: 'templateName=' + answers["template-name"] + '&templateDescription=' + answers["template-description"] + '&assetPath=' + answers["asset-path"]
    });
    var response = JSON.parse(res.getBody('utf8'));
    
    // ADD COLORED TEXT TO TERMINAL - FIRST \x1b[31m TO SET COLOR TO RED AND THEN \x1b[0m TO RESET BACK TO DEFAULT TERMINAL COLOR.
    // https://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
    if(response.error) {
        console.log("\x1b[31m", response.msg, "\x1b[0m");
    } else {
        console.log("\x1b[32m", response.msg, "\x1b[0m");
    }
}).catch(error => {
    if(error.isTtyError) {
        // Prompt couldn't be rendered in the current environment
    } else {
        // Something else when wrong
    }
});